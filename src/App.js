import React, { Component } from "react";
import "../src/App.scss";
import Button from "./components/ButtonComponent";
import Modal from "./components/ModalComponent";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalFirstOpen: false,
      modalSecondOpen: false,
    };
  }

  isModalFirstOpen = () => {
    this.setState({ modalFirstOpen: true });
  };

  isModalSecondOpen = () => {
    this.setState({ modalSecondOpen: true });
  };

  handleCloseModal = () => {
    this.setState({
      modalFirstOpen: false,
      modalSecondOpen: false,
    });
  };

  handleClickOutsideModal = (e) => {
    if (e.target.classList.contains("modal-container")) {
    this.handleCloseModal()
    }
  };

  render() {
    return (
      <div>
        <Button
          backgroundColor="#e94b35"
          text="Open first modal"
          onClick={this.isModalFirstOpen}
        />
        <Button
          backgroundColor="#3a8d7d"
          text="Open second modal"
          onClick={this.isModalSecondOpen}
        />
        {this.state.modalFirstOpen && (
          <Modal
            header="Do you want to delete this file?"
            closeButton={true}
            closeModal={this.handleCloseModal}
            text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
            actions={
              <>
                <button className="confirmBtn">Ok</button>
                <button className="cancelBtn" onClick={this.handleCloseModal}>
                  Cancel
                </button>
              </>
            }
            clickOutside={this.handleClickOutsideModal}
          />
        )}
        {this.state.modalSecondOpen && (
          <Modal
            header="Warning!"
            closeButton={true}
            closeModal={this.handleCloseModal}
            text="There is a large chance that using of unauthorized software on your machine could infect your computer! "
            actions={
              <>
                <button className="confirmBtn">Continue</button>
                <button className="cancelBtn" onClick={this.handleCloseModal}>
                  Exit
                </button>
              </>
            }
            clickOutside={this.handleClickOutsideModal}
          />
        )}
      </div>
    );
  }
}

export default App;
